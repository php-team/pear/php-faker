From: Robin Gustafsson <robin@rgson.se>
Date: Thu, 22 Sep 2022 21:30:19 +0200
Subject: Skip incompatible test cases on 32-bit systems

Several test cases are incompatible with 32-bit systems due to
datetimes that cannot fit in the 32-bit epoch.

    strtotime(): Epoch doesn't fit in a PHP integer

This change skips those test cases.

Forwarded: https://github.com/FakerPHP/Faker/pull/518
---
 test/Faker/Core/DateTimeTest.php         |  4 ++++
 test/Faker/Provider/DateTimeTest.php     |  2 ++
 test/Faker/Provider/fi_FI/PersonTest.php |  2 +-
 test/Faker/Provider/kk_KZ/PersonTest.php |  2 ++
 test/Faker/Provider/ro_RO/PersonTest.php |  6 ++++++
 test/Faker/TestCase.php                  | 12 ++++++++++++
 6 files changed, 27 insertions(+), 1 deletion(-)

diff --git a/test/Faker/Core/DateTimeTest.php b/test/Faker/Core/DateTimeTest.php
index 4b7da9f..1a9c555 100644
--- a/test/Faker/Core/DateTimeTest.php
+++ b/test/Faker/Core/DateTimeTest.php
@@ -43,6 +43,8 @@ final class DateTimeTest extends TestCase
 
     public function testDateTimeAD()
     {
+        $this->skipOn32bitSystem();
+
         $dateTime = $this->extension->dateTimeAD('2012-04-12T19:22:23');
 
         self::assertInstanceOf(\DateTime::class, $dateTime);
@@ -122,6 +124,8 @@ final class DateTimeTest extends TestCase
 
     public function testDate()
     {
+        $this->skipOn32bitSystem();
+
         $date = $this->extension->date('Y-m-d', '2102-11-12T14:45:29');
 
         self::assertIsString($date);
diff --git a/test/Faker/Provider/DateTimeTest.php b/test/Faker/Provider/DateTimeTest.php
index 86e076b..a0cafc5 100644
--- a/test/Faker/Provider/DateTimeTest.php
+++ b/test/Faker/Provider/DateTimeTest.php
@@ -238,6 +238,8 @@ final class DateTimeTest extends TestCase
 
     public function testFixedSeedWithMaximumTimestamp()
     {
+        $this->skipOn32bitSystem();
+
         $max = '2118-03-01 12:00:00';
 
         mt_srand(1);
diff --git a/test/Faker/Provider/fi_FI/PersonTest.php b/test/Faker/Provider/fi_FI/PersonTest.php
index 88b27ce..fe56e40 100644
--- a/test/Faker/Provider/fi_FI/PersonTest.php
+++ b/test/Faker/Provider/fi_FI/PersonTest.php
@@ -35,7 +35,7 @@ final class PersonTest extends TestCase
 
     public function testPersonalIdentityNumberGeneratesCompliantNumbers()
     {
-        if (strtotime('1800-01-01 00:00:00')) {
+        if (!$this->is32bitSystem()) {
             $min = '1900';
             $max = '2099';
 
diff --git a/test/Faker/Provider/kk_KZ/PersonTest.php b/test/Faker/Provider/kk_KZ/PersonTest.php
index dadde4e..a01ca87 100644
--- a/test/Faker/Provider/kk_KZ/PersonTest.php
+++ b/test/Faker/Provider/kk_KZ/PersonTest.php
@@ -18,6 +18,8 @@ final class PersonTest extends TestCase
      */
     public function testIndividualIdentificationNumberIsValid()
     {
+        $this->skipOn32bitSystem();
+
         // 21st century.
         $birthDate = DateTime::dateTimeBetween('2000-01-01', '2099-12-31');
         $individualIdentificationNumber = $this->faker->individualIdentificationNumber($birthDate, Person::GENDER_MALE);
diff --git a/test/Faker/Provider/ro_RO/PersonTest.php b/test/Faker/Provider/ro_RO/PersonTest.php
index 77ca19a..e10b5e6 100644
--- a/test/Faker/Provider/ro_RO/PersonTest.php
+++ b/test/Faker/Provider/ro_RO/PersonTest.php
@@ -13,6 +13,12 @@ final class PersonTest extends TestCase
 {
     public const TEST_CNP_REGEX = '/^[1-9][0-9]{2}(?:0[1-9]|1[012])(?:0[1-9]|[12][0-9]|3[01])(?:0[1-9]|[123][0-9]|4[0-6]|5[12])[0-9]{3}[0-9]$/';
 
+    protected function setUp(): void
+    {
+        parent::setUp();
+        $this->skipOn32bitSystem();
+    }
+
     public function invalidGenderProvider()
     {
         return [
diff --git a/test/Faker/TestCase.php b/test/Faker/TestCase.php
index 7ad6441..75a0069 100644
--- a/test/Faker/TestCase.php
+++ b/test/Faker/TestCase.php
@@ -51,4 +51,16 @@ abstract class TestCase extends BaseTestCase
     {
         return [];
     }
+
+    protected function is32bitSystem(): bool
+    {
+        return PHP_INT_SIZE === 4;
+    }
+
+    protected function skipOn32bitSystem(): void
+    {
+        if ($this->is32bitSystem()) {
+            self::markTestSkipped('Test case is incompatible with 32-bit systems.');
+        }
+    }
 }
